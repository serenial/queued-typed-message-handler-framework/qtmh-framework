# Queued Typed-Message Handler

An experimental framework which aims to add Type-Safety into the Queued Message Handling paradigm.

## Motivation
Large applications are built up from small components which must communicate with each other; This communication should happen through carefully built APIs but the necessity to flatten data-types into type-agnostic messages can cause problems as the modular-components change over time.

This framework explores one possible route to overcome this issue via strictly-typed User Event messaging. The concept itself is relatively straight forward but as User Events can require considerable programming effort to create, the main thrust of the framework is to remove the programming overhead.

The framework also represents the many design and style decisions taken to provide a logical and consistent approach to object ownership and lifetime which can trouble applications built from asynchronous components.

## Key Concepts
User Events provide some additional functionality compared to conventional LabVIEW queues. Whilst the creation and sending of a message are similar for User Events and Queues, the handling of messages is quite different. The Queue API provides various dequeue options to get a message from the queue but for User Events, messages are handled using the Event-Structure. This resembles a case structure except that it can be _dynamically_ "subscribed" to User-Event messages. 

As an Event-Structure can be subscribed to many different User Events it is possible to handle different message-queues with a single mechanism with ease. By creating a User-Event for every message, it is possible to enforce a specific data-type for each message and avoid the need to flatten a message to some generic type.

## Inspiration
This framework was inspired by the following;
* [Delacor's DQMH](https://delacor.com/products/dqmh/) 
* [Composed Systems' Stream (Event Driven) Framework](https://bitbucket.org/composedsystems/stream/src/master/)
* [James Powell's Messenger Library](https://lavag.org/files/file/220-messenger-library/)
* [Events for UI Actor Indicators](https://forums.ni.com/t5/Actor-Framework-Documents/Events-for-UI-Actor-Indicators/tac-p/3869262)



## Contributions
Contributions are welcome - simply submit a PR.

LabVIEW 2015 is required for development.
